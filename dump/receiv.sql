-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 18-Jan-2022 às 04:14
-- Versão do servidor: 10.4.22-MariaDB
-- versão do PHP: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `receiv`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `devedores`
--

CREATE TABLE `devedores` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `cpf_cnpj` varchar(200) NOT NULL,
  `nascimento` date NOT NULL,
  `endereco` text NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `cep` varchar(50) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `devedores`
--

INSERT INTO `devedores` (`id`, `nome`, `cpf_cnpj`, `nascimento`, `endereco`, `bairro`, `cidade`, `estado`, `cep`, `updated_at`, `created_at`, `deleted_at`) VALUES
(1, 'Murilo Santos', '063.000.000-07', '1994-12-17', 'Rua gentil de Ouro 615', 'Inhoaíba', 'Rio de Janeiro', 'RJ', '23063-340', '2022-01-15 00:46:23', '2022-01-15 00:46:23', NULL),
(2, 'Sabryna Souza', '013.145.245-45', '1994-12-01', '', '', '', '', '', '2022-01-15 01:11:59', '2022-01-15 01:11:59', NULL),
(3, '', '', '0000-00-00', '', '', '', '', '', '2022-01-15 02:28:19', '2022-01-15 02:28:19', '2022-01-15 06:01:23'),
(4, 'Luna', '063.000.000-07', '0000-00-00', 'Rua gentil de Ouro 615', 'Inhoaíba', 'Rio de Janeiro', '', '23063-340', '2022-01-15 11:11:01', '2022-01-15 11:11:01', '2022-01-15 04:01:19'),
(5, 'Luna Valentina', '013.145.245-45', '1994-12-17', 'Rua gentil de Ouro 615', 'Inhoaíba', 'Rio de Janeiro', 'RJ', '23063-340', '2022-01-15 11:33:43', '2022-01-15 11:33:43', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dividas`
--

CREATE TABLE `dividas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `valor` float NOT NULL,
  `data_vencimento` date NOT NULL,
  `id_devedor` int(11) DEFAULT NULL,
  `pago` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 = não pago, 1 pago',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `dividas`
--

INSERT INTO `dividas` (`id`, `titulo`, `valor`, `data_vencimento`, `id_devedor`, `pago`, `updated_at`, `created_at`, `deleted_at`) VALUES
(1, 'Luz', 280.8, '2022-02-17', 2, '0', '2022-01-17 15:52:09', '2022-01-17 12:20:10', NULL),
(2, 'Gás', 108.99, '2022-02-17', 5, '0', '2022-01-17 12:33:56', '2022-01-17 12:20:10', '2022-01-17 04:01:56'),
(3, 'Gás', 45.9, '2022-01-17', 1, '0', '2022-01-17 12:34:34', '2022-01-17 12:34:34', NULL),
(4, 'Gás', 45.9, '2022-02-17', 1, '0', '2022-01-17 12:38:11', '2022-01-17 12:37:56', '2022-01-17 04:01:11'),
(5, 'Luz', 280.8, '2022-01-17', 5, '1', '2022-01-17 23:16:07', '2022-01-17 13:37:23', NULL),
(6, 'Escola', 480, '2022-02-20', 5, '1', '2022-01-17 23:17:28', '2022-01-17 14:38:04', NULL);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `devedores`
--
ALTER TABLE `devedores`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `dividas`
--
ALTER TABLE `dividas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `devedores`
--
ALTER TABLE `devedores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `dividas`
--
ALTER TABLE `dividas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
