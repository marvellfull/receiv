<?php
interface Required {
  public function getAll();
  public function getById($id);
  public function insert($value);
  public function update($value);
  public function delete($value);
}
 ?>
