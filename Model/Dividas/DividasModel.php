<?php
include_once("Model/Dividas/DividasVO.php");
include_once("Model/Dividas/DividasDAO.php");
include_once("Model/DB.php");

class DividasModel
{

  public function insertModel($value)
  {
    $dividas = new DividasVO();
    $dividasDAO = new DividasDAO();

    return $dividasDAO->insert($value);
  }

  public function getAllByDevedorModel($id){
    $dividas = new DividasDAO();

    return $dividas->getAllByDevedor($id);
  }

  public function getByIdModel($id){
    $dividas = new DividasDAO();
    $vo = $dividas->getById($id);

    return $vo;
  }

  public function updateModel($value)
  {
    $dividas = new DividasDAO();

    return $dividas->update($value);
  }

  public function pagarModel($value)
  {
    $dividas = new DividasDAO();

    return $dividas->pagar($value);
  }

  public function deleteModel($value)
  {
    $dividas = new DividasDAO();

    return $dividas->delete($value);
  }
}

 ?>
