<?php
include_once 'Model/DB.php';
include_once 'Model/Required.php';

class DividasDAO implements Required{
  public $table = "dividas";

  public function getAll(){

  }

  public function getAllByDevedor($id){
    $sql = "SELECT * FROM $this->table WHERE id_devedor = :id_devedor AND deleted_at IS NULL";
    $stmt = DB::prepare($sql);
    $stmt->bindParam(':id_devedor', $id, PDO::PARAM_INT);
    $query = $stmt->execute();

    return $stmt->fetchAll();
  }

  public function getById($id){
    $sql = "SELECT * FROM $this->table where id = :id limit 1";
    $stmt = DB::prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $query = $stmt->execute();

    $vo = new DividasVO();

    while ($reg = $stmt->fetch()){
      $vo->setId($reg->id);
      $vo->setTitulo($reg->titulo);
      $vo->setValor($reg->valor);
      $vo->setData_vencimento($reg->data_vencimento);
      $vo->setId_devedor($reg->id_devedor);
    
    }

    return $vo;
  }

  public function insert($value){
    $sql = "INSERT INTO $this->table (titulo, valor, data_vencimento, id_devedor) VALUES (:titulo, :valor, :data_vencimento, :id_devedor)";
    $stmt = DB::prepare($sql);
    $stmt->bindValue(':titulo', $value->getTitulo());
    $stmt->bindValue(':valor', $value->getValor());
    $stmt->bindValue(':data_vencimento', $value->getData_vencimento());
    $stmt->bindValue(':id_devedor', $value->getId_devedor());


    if ($stmt->execute()) {
      return true;
    }else{
      return false;
    }
  }

  public function update($value){
    $sql = "UPDATE $this->table set titulo = :titulo, valor = :valor, data_vencimento = :data_vencimento where id = :id";
    $stmt = DB::prepare($sql);
    $stmt->bindValue(':titulo', $value->getTitulo());
    $stmt->bindValue(':valor', $value->getValor());
    $stmt->bindValue(':data_vencimento', $value->getData_vencimento());
    $stmt->bindValue(':id', $value->getId());

    if ($stmt->execute()) {
      return true;
    }else{
      return false;
    }
  }

  public function pagar($value){
    $sql = "UPDATE $this->table set pago = '1' where id = :id";
    $stmt = DB::prepare($sql);
    $stmt->bindValue(':id', $value->getId());

    if ($stmt->execute()) {
      return true;
    }else{
      return false;
    }
  }

  public function delete($value){
    $now = date('Y-m-d h:m:s');

    $sql = "UPDATE $this->table set deleted_at = :deleted_at where id = :id";
    $stmt = DB::prepare($sql);
    $stmt->bindValue(':id', $value->getId());
    $stmt->bindValue(':deleted_at', $now);

    if ($stmt->execute()) {
      return true;
    }else{
      return false;
    }
  }

}

 ?>
