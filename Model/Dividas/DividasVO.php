<?php
class DividasVO{
    private $id;
    private $titulo;
    private $valor;
    private $data_vencimento;
    private $id_devedor;
    private $updated_at;
    private $created_at;
    private $deleted_at;

    public function setId($id){
      $this->id = $id;
    }

    public function getId(){
      return $this->id;
    }

    public function setTitulo($titulo){
      $this->titulo = $titulo;
    }

    public function getTitulo(){
      return $this->titulo;
    }

    public function setValor($valor){
      $this->valor = $valor;
    }

    public function getValor(){
      return $this->valor;
    }

    public function setData_vencimento($data_vencimento){
      $this->data_vencimento = $data_vencimento;
    }

    public function getData_vencimento(){
      return $this->data_vencimento;
    }

    public function setId_devedor($id_devedor){
      $this->id_devedor = $id_devedor;
    }

    public function getId_devedor(){
      return $this->id_devedor;
    }

    public function setUpdatedAt($updated_at){
      $this->updated_at = $updated_at;
    }

    public function getUpdatedAt(){
      return $this->updated_at;
    }

    public function setCreatedAt($created_at){
      $this->created_at = $created_at;
    }

    public function getCreatedAt(){
      return $this->created_at;
    }

    public function setDeletedAt($deleted_at){
      $this->deleted_at = $deleted_at;
    }

    public function getDeletedAt(){
      return $this->deleted_at;
    }
}

 ?>
