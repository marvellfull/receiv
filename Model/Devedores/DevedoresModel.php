<?php
include_once("Model/Devedores/DevedoresVO.php");
include_once("Model/Devedores/DevedoresDAO.php");
include_once("Model/DB.php");

class DevedoresModel
{

  public function insertModel($value)
  {
    $devedores = new DevedoresVO();
    $devedoresDAO = new DevedoresDAO();

    return $devedoresDAO->insert($value);
  }

  public function getAllModel(){
    $devedores = new DevedoresDAO();

    return $devedores->getAll();
  }

  public function getByIdModel($id){
    $devedores = new DevedoresDAO();
    $vo = $devedores->getById($id);

    return $vo;
  }

  public function updateModel($value)
  {
    $devedores = new DevedoresDAO();

    return $devedores->update($value);
  }

  public function deleteModel($value)
  {
    $devedores = new DevedoresDAO();

    return $devedores->delete($value);
  }
}

 ?>
