<?php
class DevedoresVO{
    private $id;
    private $name;
    private $cpf_cnpj;
    private $nascimento;
    private $endereco;
    private $bairro;
    private $cidade;
    private $estado;
    private $cep;
    private $updated_at;
    private $created_at;
    private $deleted_at;

    public function setId($id){
      $this->id = $id;
    }

    public function getId(){
      return $this->id;
    }

    public function setNome($name){
      $this->name = $name;
    }

    public function getNome(){
      return $this->name;
    }

    public function setCpf_cnpj($cpf_cnpj){
      $this->cpf_cnpj = $cpf_cnpj;
    }

    public function getCpf_cnpj(){
      return $this->cpf_cnpj;
    }

    public function setNascimento($nascimento){
      $this->nascimento = $nascimento;
    }

    public function getNascimento(){
      return $this->nascimento;
    }

    public function setEndereco($endereco){
      $this->endereco = $endereco;
    }

    public function getEndereco(){
      return $this->endereco;
    }

    public function setBairro($bairro){
      $this->bairro = $bairro;
    }

    public function getBairro(){
      return $this->bairro;
    }

    public function setCidade($cidade){
      $this->cidade = $cidade;
    }

    public function getCidade(){
      return $this->cidade;
    }

    public function setEstado($estado){
      $this->estado = $estado;
    }

    public function getEstado(){
      return $this->estado;
    }

    public function setCep($cep){
      $this->cep = $cep;
    }

    public function getCep(){
      return $this->cep;
    }

    public function setUpdatedAt($updated_at){
      $this->updated_at = $updated_at;
    }

    public function getUpdatedAt(){
      return $this->updated_at;
    }

    public function setCreatedAt($created_at){
      $this->created_at = $created_at;
    }

    public function getCreatedAt(){
      return $this->created_at;
    }

    public function setDeletedAt($deleted_at){
      $this->deleted_at = $deleted_at;
    }

    public function getDeletedAt(){
      return $this->deleted_at;
    }
}

 ?>
