<?php include_once 'View/elements/header.php'; ?>
<div class="container">
    <h3>Devedor</h3>
  <form action="<?php echo $_SESSION["base_url"]; ?>devedores/update" method="post">
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="txtNome" class="form-label">Nome Completo</label>
        <input type="text" class="form-control" id="txtNome" name="txtNome" value="<?php echo $_SESSION['name']; ?>">
      </div>
      <div class="form-group col-md-4">
        <label for="txtCpf_cnpj" class="form-label">CPF / CNPJ</label>
        <input type="txt" class="form-control" name="txtCpf_cnpj" id="txtCpf_cnpj" value="<?php echo $_SESSION['cpf_cnpj']; ?>">
      </div>
      <div class="form-group col-md-2">
        <label for="txtNascimento" class="form-label">Nascimento</label>
        <input type="date" class="form-control" id="txtNascimento" name="txtNascimento"  value="<?php echo $_SESSION['nascimento']; ?>">
      </div>
      <div class="form-group col-md-6">
      <label for="txtEndereco" class="form-label">Endereço</label>
      <input type="text" class="form-control" name="txtEndereco" id="txtEndereco" value="<?php echo $_SESSION['endereco']; ?>">
    </div>
    <div class="form-group col-md-6">
      <label for="txtBairro" class="form-label">Bairro</label>
      <input type="text" class="form-control" id="txtBairro" name="txtBairro" value="<?php echo $_SESSION['bairro']; ?>">
    </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-4">
        <label for="txtCidade" class="form-label">Cidade</label>
        <input type="text" class="form-control" id="txtCidade" name="txtCidade" value="<?php echo $_SESSION['cidade']; ?>">
      </div>
      <div class="form-group col-md-4">
        <label for="txtEstado" class="form-label">Estado</label>
        <select id="txtEstado" name="txtEstado"  class="form-control">
          <option selected><?php echo $_SESSION['estado']; ?></option>
          <option value="AC">Acre</option>
          <option value="AL">Alagoas</option>
          <option value="AP">Amapá</option>
          <option value="AM">Amazonas</option>
          <option value="BA">Bahia</option>
          <option value="CE">Ceará</option>
          <option value="DF">Distrito Federal</option>
          <option value="ES">Espírito Santo</option>
          <option value="GO">Goiás</option>
          <option value="MA">Maranhão</option>
          <option value="MT">Mato Grosso</option>
          <option value="MS">Mato Grosso do Sul</option>
          <option value="MG">Minas Gerais</option>
          <option value="PA">Pará</option>
          <option value="PB">Paraíba</option>
          <option value="PR">Paraná</option>
          <option value="PE">Pernambuco</option>
          <option value="PI">Piauí</option>
          <option value="RJ">Rio de Janeiro</option>
          <option value="RN">Rio Grande do Norte</option>
          <option value="RS">Rio Grande do Sul</option>
          <option value="RO">Rondônia</option>
          <option value="RR">Roraima</option>
          <option value="SC">Santa Catarina</option>
          <option value="SP">São Paulo</option>
          <option value="SE">Sergipe</option>
          <option value="TO">Tocantins</option>
          <option value="EX">Estrangeiro</option>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label for="txtCep" class="form-label">CEP</label>
        <input type="text" class="form-control" id="txtCep" name="txtCep" value="<?php echo $_SESSION['cep']; ?>">
      </div>
     
    </div>
      <input type="hidden" name="txtId" id="txtId" value="<?php echo $_SESSION['id']; ?>">
      <input type="submit" class="btn btn-primary" name="register" value="Editar">
  </form>
</div>

<div class="container" style="margin-top: 40px">
<h3>Dividas</h3>
<table class="table table-bordered">
  <thead class="table-light">
  <tr>
      <td>Id</td>
      <td>Titulo</td>
      <td>Valor</td>
      <td>Vencimento</td>
      <td>Status de pagemento</td>
      <td>Última alteração</td>
      <td>Ações</td>
    </tr>
 
  </thead>
    
    <?php 
    foreach ($_SESSION["dividas"] as $row_div): 
    ?>
      <tbody class="table-light">
      
        <td><?php echo $row_div->id; ?></td>
        <td><?php echo $row_div->titulo; ?></td>
        <td><?php echo "R$". $row_div->valor; ?></td>
        <td><?php echo date('d/m/Y', strtotime($row_div->data_vencimento)); ?></td>
        <td><?php  echo ($row_div->pago == '0')? "Não pago" : "Pago"; ?></td>
        <td><?php echo date('d/m/Y H:i:s', strtotime($row_div->updated_at)); ?></td>
        <td>
          <a class="editar-divida" href="<?php echo $_SESSION["base_url"]; ?>dividas/edit/<?php echo $row_div->id; ?>" data-toggle="modal" data-target="#ExemploModalCentralizado" data-id="<?php echo $row_div->id; ?>" data-titulo="<?php echo $row_div->titulo; ?>" data-valor="<?php echo $row_div->valor;?>" data-vencimento="<?php echo $row_div->data_vencimento; ?>">Editar</a>
          <?php if($row_div->pago == '0'){ ?>
          <a href="<?php echo $_SESSION["base_url"]; ?>dividas/pagar/<?php echo $row_div->id; ?>">Pagar dívida</a>
          <?php } ?>
          <a href="<?php echo $_SESSION["base_url"]; ?>dividas/delete/<?php echo $row_div->id; ?>">Deletar</a>
         
        </td>
    </tbody>
      
    <?php endforeach; ?>
  </table>
</div>

   <!-- Modal dívida-->
<div class="modal fade" id="ExemploModalCentralizado" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Editar dívida</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo $_SESSION["base_url"]; ?>dividas/update"  method="post"> 
          <div class="form-group">
            <label for="txtTitulo">Títutlo da dívida</label>
            <input type="text" class="form-control" id="txtTitulo" name="txtTitulo"  value="">
          </div>
          <div class="form-group">
            <label for="txtValor">Valor</label>
            <input type="txt" class="form-control" id="txtValor" name="txtValor"  value="">
          </div>
          <div class="form-group">
            <label for="txtData_vencimento">Data de Vencimento</label>
            <input type="date" class="form-control" id="txtData_vencimento" name="txtData_vencimento" value="">
          </div>
          <input type="hidden" name="txtId" id="txtIdModal" value="">
          <input type="hidden" name="txtIdDevedor" id="txtIdDevedor" value="<?php echo $_SESSION['id']; ?>">
          <input type="submit" class="btn btn-primary" name="register" value="Salvar">
        </form>
      </div>
    </div>
  </div>
</div>

<?php include_once 'View/elements/footer.php'; ?>
