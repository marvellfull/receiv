<?php include_once 'View/elements/header.php'; ?>
  <h2 class="text-center">Lista de devedores</h2>
  <div class="m-1">
  <a href="<?php echo $_SESSION["base_url"]; ?>devedores/new" class="btn btn-primary">Inserir devedor</a>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ExemploModalCentralizado">
  Inserir Dívidas</button>
  </div>
  <table class="table table-bordered">
  <thead class="table-light">
  <tr>
      <td>Id</td>
      <td>Nome</td>
      <td>CPF / CNPJ</td>
      <td>Nascimento</td>
      <td>Última alteração</td>
      <td>Ações</td>
    </tr>
 
  </thead>
    
    <?php foreach ($_SESSION["devedores"] as $row): ?>
      <tbody class="table-light">
      
        <td><?php echo $row->id; ?></td>
        <td><?php echo $row->nome; ?></td>
        <td><?php echo $row->cpf_cnpj; ?></td>
        <td><?php echo date('d/m/Y', strtotime($row->nascimento)); ?></td>
        <td><?php echo date('d/m/Y H:i:s', strtotime($row->updated_at)); ?></td>
        <td>
          <a href="<?php echo $_SESSION["base_url"]; ?>devedores/edit/<?php echo $row->id; ?>"> Ver</a>
          <a href="<?php echo $_SESSION["base_url"]; ?>devedores/delete/<?php echo $row->id; ?>">Deletar</a>
        </td>
    </tbody>
      
    <?php endforeach; ?>
  </table>

  <!-- Modal dívida-->
<div class="modal fade" id="ExemploModalCentralizado" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Inserir dívida</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo $_SESSION["base_url"]; ?>dividas/save"  method="post"> 
          <div class="form-group">
            <label for="txtTitulo">Títutlo da dívida</label>
            <input type="text" class="form-control" id="txtTitulo" name="txtTitulo">
          </div>
          <div class="form-group">
            <label for="txtValor">Valor</label>
            <input type="int" class="form-control" id="txtValor" name="txtValor" placeholder="R$99.90">
          </div>
          <div class="form-group">
            <label for="txtData_vencimento">Data de Vencimento</label>
            <input type="date" class="form-control" id="txtData_vencimento" name="txtData_vencimento">
          </div>
          <div class="form-group">
            <label for="txtId_devedor" class="form-label">Devedor</label>
            <select id="txtId_devedor" name="txtId_devedor"  class="form-control">
            <?php foreach ($_SESSION["devedores"] as $row): ?>
              <option value="<?php echo $row->id; ?>"><?php echo $row->nome; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <input type="submit" class="btn btn-primary" name="register" value="Cadastrar">
        </form>
      </div>
    </div>
  </div>
</div>
<?php include_once 'View/elements/footer.php'; ?>
