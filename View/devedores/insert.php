
<?php include_once 'View/elements/header.php'; ?>
<h2 class="text-center">Inserir Devedor</h2>

<div class="container">
<form action="<?php echo $_SESSION["base_url"]; ?>devedores/save" method="post">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="txtNome" class="form-label">Nome Completo</label>
      <input type="text" class="form-control" id="txtNome" name="txtNome">
    </div>
    <div class="form-group col-md-4">
      <label for="txtCpf_cnpj" class="form-label">CPF / CNPJ</label>
      <input type="txt" class="form-control" name="txtCpf_cnpj" id="txtCpf_cnpj">
    </div>
    <div class="form-group col-md-2">
      <label for="txtNascimento" class="form-label">Nascimento</label>
      <input type="date" class="form-control" id="txtNascimento" name="txtNascimento">
    </div>
    <div class="form-group col-md-6">
      <label for="txtEndereco" class="form-label">Endereço</label>
      <input type="text" class="form-control" name="txtEndereco" id="txtEndereco"  placeholder="">
    </div>
    <div class="form-group col-md-6">
      <label for="txtBairro" class="form-label">Bairro</label>
      <input type="text" class="form-control" id="txtBairro" name="txtBairro" placeholder="">
    </div>
  </div>
  
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="txtCidade" class="form-label">Cidade</label>
      <input type="text" class="form-control" id="txtCidade" name="txtCidade">
    </div>
    <div class="form-group col-md-4">
      <label for="txtEstado" class="form-label">Estado</label>
      <select id="txtEstado" name="txtEstado" class="form-control">
        <option value="AC">Acre</option>
        <option value="AL">Alagoas</option>
        <option value="AP">Amapá</option>
        <option value="AM">Amazonas</option>
        <option value="BA">Bahia</option>
        <option value="CE">Ceará</option>
        <option value="DF">Distrito Federal</option>
        <option value="ES">Espírito Santo</option>
        <option value="GO">Goiás</option>
        <option value="MA">Maranhão</option>
        <option value="MT">Mato Grosso</option>
        <option value="MS">Mato Grosso do Sul</option>
        <option value="MG">Minas Gerais</option>
        <option value="PA">Pará</option>
        <option value="PB">Paraíba</option>
        <option value="PR">Paraná</option>
        <option value="PE">Pernambuco</option>
        <option value="PI">Piauí</option>
        <option value="RJ">Rio de Janeiro</option>
        <option value="RN">Rio Grande do Norte</option>
        <option value="RS">Rio Grande do Sul</option>
        <option value="RO">Rondônia</option>
        <option value="RR">Roraima</option>
        <option value="SC">Santa Catarina</option>
        <option value="SP">São Paulo</option>
        <option value="SE">Sergipe</option>
        <option value="TO">Tocantins</option>
        <option value="EX">Estrangeiro</option>
      </select>
    </div>
    <div class="form-group col-md-2">
      <label for="txtCep" class="form-label">CEP</label>
      <input type="text" class="form-control" id="txtCep" name="txtCep">
    </div>
  </div>
  <input type="submit" class="btn btn-primary" name="register" value="Cadastrar">
</form>
</div>
<?php include_once 'View/elements/footer.php'; ?>

