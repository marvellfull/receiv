<?php

include_once("Model/Dividas/DividasModel.php");
include_once("Model/Dividas/DividasVO.php");
include_once("Model/Dividas/DividasDAO.php");

class DividasController
{

  function __construct()
  {

  }

  public function DividasController(){

  }

public function save(){
  $model = new DividasModel();
  $vo = new DividasVO();

  $vo->settitulo($_POST["txtTitulo"]);
  $vo->setvalor($_POST["txtValor"]);
  $vo->setData_vencimento($_POST["txtData_vencimento"]);
  $vo->setid_devedor($_POST["txtId_devedor"]);
  

  if($model->insertModel($vo)){
    $_SESSION["msg"] = "Dívida cadastrado com sucesso";
  } else {
    $_SESSION["msg"] = "Erro ao cadastrar o divida";
  }

  header("Location: ".$_SESSION["base_url"]."devedores/list");
}


public function update(){
  $model = new DividasModel();
  $vo = new DividasVO();

  $vo->setId($_POST["txtId"]);
  $vo->setTitulo($_POST["txtTitulo"]);
  $vo->setValor($_POST["txtValor"]);
  $vo->setData_vencimento($_POST["txtData_vencimento"]);

  if($model->updateModel($vo)){
    $_SESSION["msg"] = "Divida atualizado com sucesso";
  } else {
    $_SESSION["msg"] = "Erro ao atualizar o Divida";
  }

  header("Location: ".$_SESSION["base_url"]."devedores/edit/".$_POST["txtIdDevedor"]);
}


public function pagar(){
  $model = new DividasModel();
  $vo = new DividasVO();

  $vo->setId($_GET["id"]);

  if($model->pagarModel($vo)){
    $_SESSION["msg"] = "Divida atualizado com sucesso";
  } else {
    $_SESSION["msg"] = "Erro ao atualizar o divida";
  }

  header("Location: ".$_SESSION["base_url"]."devedores/list");
}

public function delete(){
  $model = new DividasModel();
  $vo = new DividasVO();

  $vo->setId($_GET["id"]);

  if($model->deleteModel($vo)){
    $_SESSION["msg"] = "Divida atualizado com sucesso";
  } else {
    $_SESSION["msg"] = "Erro ao atualizar o divida";
  }

  header("Location: ".$_SESSION["base_url"]."devedores/list");
}

public function __destruct(){
  // return self::getInstance()->close();
}

}
