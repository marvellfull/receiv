<?php
include_once("Model/Devedores/DevedoresModel.php");
include_once("Model/Devedores/DevedoresVO.php");
include_once("Model/Devedores/DevedoresDAO.php");

include_once("Model/Dividas/DividasModel.php");
include_once("Model/Dividas/DividasVO.php");
include_once("Model/Dividas/DividasDAO.php");

class DevedoresController
{

  function __construct()
  {

  }

  public function DevedoresController(){

  }

  public function list(){
    $model = new DevedoresModel();
    $_SESSION["devedores"] = $model->getAllModel();
    include("View/devedores/list.php");
  }

  public function save(){
    $model = new DevedoresModel();
    $vo = new DevedoresVO();

    $vo->setNome($_POST["txtNome"]);
    $vo->setcpf_cnpj($_POST["txtCpf_cnpj"]);
    $vo->setnascimento($_POST["txtNascimento"]);
    $vo->setendereco($_POST["txtEndereco"]);
    $vo->setbairro($_POST["txtBairro"]);
    $vo->setcidade($_POST["txtCidade"]);
    $vo->setestado($_POST["txtEstado"]);
    $vo->setcep($_POST["txtCep"]);
    

    if($model->insertModel($vo)){
      $_SESSION["msg"] = "Devedor cadastrado com sucesso";
    } else {
      $_SESSION["msg"] = "Erro ao cadastrar o devedor";
    }

    header("Location: list");
  }

  public function new(){
    include("View/devedores/insert.php");
  }

  public function edit(){
    $model = new DevedoresModel();
    $vo = $model->getByIdModel($_GET["id"]);
    $_SESSION["id"] = $vo->getId();
    $_SESSION["name"] = $vo->getNome();
    $_SESSION["cpf_cnpj"] = $vo->getCpf_cnpj();
    $_SESSION["nascimento"] = $vo->getNascimento();
    $_SESSION["endereco"] = $vo->getEndereco();
    $_SESSION["bairro"] = $vo->getBairro();
    $_SESSION["cidade"] = $vo->getCidade();
    $_SESSION["estado"] = $vo->getEstado();
    $_SESSION["cep"] = $vo->getCep();

    $model = new DividasModel();
    $_SESSION["dividas"] = $model->getAllByDevedorModel($_GET["id"]);
    
    include("View/devedores/edit.php");
  }

  public function update(){
    $model = new DevedoresModel();
    $vo = new DevedoresVO();

    $vo->setId($_POST["txtId"]);
    $vo->setNome($_POST["txtNome"]);
    $vo->setcpf_cnpj($_POST["txtCpf_cnpj"]);
    $vo->setnascimento($_POST["txtNascimento"]);
    $vo->setendereco($_POST["txtEndereco"]);
    $vo->setbairro($_POST["txtBairro"]);
    $vo->setcidade($_POST["txtCidade"]);
    $vo->setestado($_POST["txtEstado"]);
    $vo->setcep($_POST["txtCep"]);

    if($model->updateModel($vo)){
      $_SESSION["msg"] = "Produto atualizado com sucesso";
    } else {
      $_SESSION["msg"] = "Erro ao atualizar o produto";
    }

    header("Location: ".$_SESSION["base_url"]."devedores/list");
  }

  public function delete(){
    $model = new DevedoresModel();
    $vo = new DevedoresVO();

    $vo->setId($_GET["id"]);

    if($model->deleteModel($vo)){
      $_SESSION["msg"] = "Devedor atualizado com sucesso";
    } else {
      $_SESSION["msg"] = "Erro ao atualizar o devedor";
    }

    header("Location: ".$_SESSION["base_url"]."devedores/list");
  }

  public function __destruct(){
    // return self::getInstance()->close();
  }
}



 ?>
